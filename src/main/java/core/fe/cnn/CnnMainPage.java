package core.fe.cnn;

import core.AbstractPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CnnMainPage extends AbstractPage {


    @FindBy (xpath = "//div[@id='search-button']")
    private WebElement searchButton;

    @FindBy(xpath = "//input[@class='search__input-field']")
    private WebElement  searchInputField;


    public CnnMainPage(WebDriver driver) {
        super(driver);
    }

    public void getSearchField (){
        searchButton.click();
    }

    public CnnSearchResultPage searchFor(final String text) {
        searchInputField.sendKeys(text, Keys.ENTER);
        return new CnnSearchResultPage(driver);
    }

}
