package core.fe.cnn;

import core.AbstractPage;
import core.be.dto.CnnDto;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CnnSearchResultPage extends AbstractPage { //https://edition.cnn.com/search/?q=bitcoin


    @FindBys({@FindBy (xpath = "//*[@class='cnn-search__result-headline']")})
    private List<WebElement> headlines;

    @FindBys ({@FindBy(xpath = "//*[@class='cnn-search__result-body']")})
    private List<WebElement> bodies;

    @FindBy(xpath = "//*[@class='cnn-search__result cnn-search__result--article']")
    private List<WebElement> searchResults;

    public CnnSearchResultPage(WebDriver driver) {
        super(driver);
    }

    public List<String> getSearchResultTitles() {
        return headlines.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public List<String> getSearchResultBody() {
        return bodies.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public List<String> getSearchResultsList() {
        return bodies.stream()
                .map(item -> item.getText().trim())
                .collect(Collectors.toList());
    }


}
