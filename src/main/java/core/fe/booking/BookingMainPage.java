package core.fe.booking;

import core.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.time.LocalDate;
import java.util.List;

public class BookingMainPage extends AbstractPage {

    @FindBy(xpath = "//input[@type='search']")
    private WebElement destinationField;

    @FindBy(xpath = "//div[@data-mode='checkin']")
    private WebElement checkinField;

    @FindBys ( {@FindBy (xpath="///td[@class='bui-calendar__date']")})
    private List<WebElement> datesSelectionCalendar;

    @FindBy(xpath = "//div[@data-mode='checkout']")
    private WebElement checkoutField;

    @FindBy(xpath = "//td[@data-date='2019-04-10']") //specified hardcoded date
    private WebElement checkinDate;

    @FindBy(xpath = "//td[@data-date='2019-04-13']") //specified hardcoded date
    private WebElement checkoutDate;


    @FindBy(xpath ="//label[@id='xp__guests__toggle']")
    private WebElement travellerAndRoomSelection;

    @FindBy(xpath = "//div[@class='sb-group__field sb-group__field-adults']//button[@class='bui-button bui-button--secondary bui-stepper__subtract-button']")
    private WebElement adultsReduceButton;

    @FindBy(xpath="//button[@data-sb-id='main']")
    private WebElement searchButton;


    public BookingMainPage(final WebDriver driver) {
        super(driver);
    }



    public void addDestination(String destination){
        destinationField.sendKeys(destination);

    }

    public void substractAdultsQty(){
        travellerAndRoomSelection.click();
        adultsReduceButton.click();
    }



    public void selectCheckinDate (){//LocalDate date
        checkinField.click();
        checkinDate.click();



//        //need to add: xpath of month from calendar = calendarMonth
//        date.getMonth().name(); //returns month name (f.e. march)
//        LocalDate.of(2019,3,17);// user vvodit
//
//        //if calendarMonth=currentMonth

    }

    public LocalDate pickCheckinDate(int date, String month, int year){
        String currentDate = LocalDate.now().toString();
        return null;
    }

    public void selectCheckoutDate(){
      // checkoutField.click();
       checkoutDate.click();
    }



    public BookingHotelsListPage getSearchHotelsResult() { //final String text
        searchButton.click();
        return new BookingHotelsListPage(driver);
    }



}
