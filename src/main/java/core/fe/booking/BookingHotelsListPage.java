package core.fe.booking;

import core.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;
import java.util.stream.Collectors;

public class BookingHotelsListPage extends AbstractPage {

    @FindBys({@FindBy(xpath = "//*[contains(@class, 'sprite-ratings_stars') and not(contains(@class, 'g-hidden'))]")})
    private List<WebElement> hotelsRates;

    @FindBy(xpath = "//div[@id='filter_class']//a[@data-id='class-5']")
    private WebElement fiveStarsFilter;

    public BookingHotelsListPage(WebDriver driver) {
        super(driver);
    }

    public void getFiveStarsHotels(){
        this.waitForJsToLoad();
        fiveStarsFilter.click();
    }

    public List<String> getHotelsStarsList () {
        return hotelsRates.stream()
               .peek(item -> actions.moveToElement(item).build().perform())
                .map(item -> item.getAttribute("class"))
                .collect(Collectors.toList());
    }
}
