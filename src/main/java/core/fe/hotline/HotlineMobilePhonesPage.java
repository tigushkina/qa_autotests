package core.fe.hotline;

import core.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.stream.Collectors;

public class HotlineMobilePhonesPage extends AbstractPage {

    @FindBys({@FindBy(xpath="//div[@class='item-info']//p[@class='h4']")}) // bez promo-item'a //li[@class='product-item'] // s //div[@class='item-info']//p[@class='h4']
    private List<WebElement> goodsTabItems; //  //ul[@class='products-list cell-list']


    @FindBy(xpath = "//ul[@class='m_b-md']//a[@href='/mobile/mobilnye-telefony-i-smartfony/294245/']")
    private WebElement appleSelection;

    private WebDriver driver;
    private Actions actions;
    private WebDriverWait wait;

    public HotlineMobilePhonesPage(final WebDriver driver){
        super(driver);
    }
//        this.driver = driver;
//        wait = new WebDriverWait(driver, 30);
//        actions = new Actions(driver);
//        PageFactory.initElements(driver,this);
//    }

    public void setAppleSelection() {
        appleSelection.click();
        //    driver.manage().timeouts().setScriptTimeout(100, TimeUnit.SECONDS);

    }

    public List<String> getItemsList () {
        return goodsTabItems.stream()
                .peek(item -> actions.moveToElement(item).build().perform())
                .map(item -> item.getText())
                .collect(Collectors.toList());
    }






}
