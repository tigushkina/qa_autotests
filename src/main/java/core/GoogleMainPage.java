package core;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class GoogleMainPage {

    @FindBy(xpath = "//input[@name='q']")
    private WebElement searchField;

    @FindBys({@FindBy(xpath="//ul[@class='erkvQe']//div[@role='option']//span")})
    private List<WebElement> dropdownSuggestions;

    private WebDriver driver;


    public GoogleMainPage(final WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver,this);

    }

    public List<String> getDropdownItems(final String text){
        searchField.sendKeys(text);
        return dropdownSuggestions.stream()
                .map(item-> item.getText()) //or .map(WebElement::getText)
                .collect(Collectors.toList());
    }

}
