package core.be;

import core.be.dto.CnnDto;
import lombok.Data;

import java.util.List;

@Data
public class CnnResponseResult {
    private List<CnnDto> result;
}
