package core.be;

import core.be.dto.CnnDto;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class CnnApi extends AbstractApi{

    private static final String CNN_SEARCH_LINK = "https://search.api.cnn.io/content?q=bitcoin&size=10";

    public CnnResponseResult searchFor() {
        return RestAssured.given()
                .contentType(ContentType.JSON)
                //.body(CnnDto.class)
                .get(CNN_SEARCH_LINK)
                .body()
                .as(CnnResponseResult.class);


    }

}
