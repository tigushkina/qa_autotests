package oop.parking;

import lombok.Data;

import java.util.Objects;

@Data
public abstract class Vehicle {

    //Класс автомобиль должен иметь слудующие поля: id, марка, ключ, тип транспортного средства (легковой/грузовой/мотоцикл), цвет автомобиля, номерной знак

    protected int id;
    protected String carMake;
    protected String key;
    protected String type;
    protected String colour;
    protected String carPlate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return id == vehicle.id &&
                carMake.equals(vehicle.carMake) &&
                key.equals(vehicle.key) &&
                type.equals(vehicle.type) &&
                colour.equals(vehicle.colour) &&
                carPlate.equals(vehicle.carPlate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, carMake, key, type, colour, carPlate);
    }
}
