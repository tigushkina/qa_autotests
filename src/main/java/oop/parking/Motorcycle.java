package oop.parking;

import lombok.Data;

@Data
public class Motorcycle extends Vehicle {
   //Класс мотоцикл должен состоять из полей: id, марка, цвет. нормерной знак


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return id == vehicle.id &&
                carMake.equals(vehicle.carMake) &&
                key.equals(vehicle.key) &&
                carPlate.equals(vehicle.carPlate);
    }


}
