package oop.parking;

import java.util.*;

public class ParkingScanner {
    private static final int MAX_PARK_SIZE = 10;

    public static void main(String[] args) {
        List<Vehicle> parked = new ArrayList<>();
        Vehicle vehicle = new Motorcycle();
        vehicle.setColour("red");
        vehicle.setCarPlate("11111");

        Map<String, Vehicle> allowedNumbers = new HashMap<>();
        allowedNumbers.put("11111", vehicle);
        Scanner scan = new Scanner(System.in);
        String helloText = "Welcome to Parcking,Please enter you car number";
        System.out.println(helloText);
        while (scan.hasNext()) {
            if(parked.size() < MAX_PARK_SIZE) {
                String carNumber = scan.nextLine();
                System.out.println("Please enter you car type");
                String carType = scan.nextLine();
                if(carType.equals("truck")) {
                    System.out.println("Trucks not allowed");
                } else {
                    System.out.println("Please enter your car color");
                    Vehicle vehicleToPark = allowedNumbers.get(carNumber);
                }
            } else {
                System.out.println("Sory, there are no free slots for your car");
            }
            System.out.println("Do you what to continue? Y/N");
            if("N".equals(scan.nextLine())) {
                System.out.println("Good Bye!");
                System.exit(0);
            } else {
                System.out.println(helloText);
            }

        }

    }
}
