package oop.parking;

public class Truck extends Vehicle {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return id == vehicle.id &&
                carMake.equals(vehicle.carMake) &&
                key.equals(vehicle.key) &&
                type.equals(vehicle.type) &&
                colour.equals(vehicle.colour) &&
                carPlate.equals(vehicle.carPlate);
    }
}
