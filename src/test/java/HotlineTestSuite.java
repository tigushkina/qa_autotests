import core.fe.hotline.HotlineMobilePhonesPage;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class HotlineTestSuite extends BaseTest{

    @Test
    public void checkHotlineMobilePhonesPageTest() {
        webDriver.get("https://hotline.ua/mobile/mobilnye-telefony-i-smartfony/"); ////1. Зайти на сайт https://hotline.ua 2. Открыть следующую ссылку /mobile/mobilnye-telefony-i-smartfony/String actualTitle = webDriver().getTitle();
        final HotlineMobilePhonesPage phonesPage = new HotlineMobilePhonesPage(webDriver);
        String actualTitle = webDriver.getTitle();
        String expectedTitle= "Смартфоны и мобильные телефоны на HOTLINE - купить | выгодные цены в Киеве, Харькове, Днепре, Одессе";
        Assert.assertEquals("title is not matching an expected one", actualTitle, expectedTitle); //3. Проверить, что стртаница с с мобильными телефонами успешно открыта
    }

    @Test
    public void checkApplePhonesTest()
    {
        webDriver.get("https://hotline.ua/mobile/mobilnye-telefony-i-smartfony/");
        final HotlineMobilePhonesPage phonesPage = new HotlineMobilePhonesPage(webDriver);
        phonesPage.setAppleSelection(); //4. В колонке Производитель выбрать Apple
        phonesPage.waitForJsToLoad();

        final List<String> filteredResults = phonesPage.getItemsList();

        Assert.assertFalse("There are no filter items available!", filteredResults.isEmpty());

        filteredResults.forEach(item -> {
            final String filterItemText = String.format("There is incorrect filter item text displayed! Expected [%s], Actual [%s]", "Apple", item);
            Assert.assertTrue(filterItemText, item.toLowerCase().contains("apple")); //5. Проверить, что в списке результататов осталиьс только смартфоны, производитель которых Apple
        });

    }




}
