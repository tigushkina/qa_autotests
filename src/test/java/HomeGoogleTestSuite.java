import core.GoogleMainPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HomeGoogleTestSuite {

    private WebDriver webDriver;

    @Before
    public void driverSetUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);//wait for page loading for 10 sec
        webDriver.manage().window().maximize();//maximize browser page size

    }

    @Test
    public void checkGoogleDropDownTest() {
        webDriver.get("https://www.google.com/");
        WebElement searchField = webDriver.findElement(By.xpath("//input[@name='q']"));
        //  searchField.click();
        searchField.sendKeys("hillel");
        List<WebElement> dropdownItems = webDriver.findElements(By.xpath("//ul[@class='erkvQe']//div[@role='option']//span"));//ul[@class='erkvQe']//div[@class='sbtc']//span

        String hillel = "hillel";

        for (WebElement searchItem : dropdownItems) {
            Assert.assertTrue("no matching hillel", searchItem.getText().contains(hillel));

        }
    }

        @Test
        public void checkGoogleSearchTestUsingOOP()
        {
            webDriver.get("https://www.google.com/");
            final GoogleMainPage mainPage = new GoogleMainPage(webDriver);

            final List<String> searchResults = mainPage.getDropdownItems("hillel");

            searchResults.forEach(item->{
                Assert.assertTrue("failed test - no matches", item.toLowerCase().contains("hillel"));
            });
        }



    @After
    public void driverTearDown() {
        webDriver.close(); //close browser
        webDriver.quit(); //stop process
    }
}