import core.fe.booking.BookingHotelsListPage;
import core.fe.booking.BookingMainPage;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class BookingTestSuite extends BaseTest{

    @Test
    public void checkHotelsResult(){
        webDriver.get("https://www.booking.com/index.ru.html");
        final BookingMainPage mainPage= new BookingMainPage(webDriver);
        mainPage.addDestination("Рим");
        mainPage.waitForJsToLoad();
        mainPage.selectCheckinDate();
        mainPage.selectCheckoutDate();
        mainPage.substractAdultsQty();

        final BookingHotelsListPage hotelsListPage = mainPage.getSearchHotelsResult();
        hotelsListPage.waitForJsToLoad();

        hotelsListPage.getFiveStarsHotels();
        hotelsListPage.waitForJsToLoad();

        final List<String> starsRates = hotelsListPage.getHotelsStarsList();

        starsRates.forEach(item -> {
            Assert.assertTrue(item.toLowerCase().contains("5"));
        });


    }

}
