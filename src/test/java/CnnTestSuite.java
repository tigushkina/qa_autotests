import core.be.CnnApi;
import core.be.CnnResponseResult;
import core.be.dto.CnnDto;
import core.fe.cnn.CnnMainPage;
import core.fe.cnn.CnnSearchResultPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class CnnTestSuite extends BaseTest {


    @Test
    public void checkCnnApi(){
        webDriver.get("https://edition.cnn.com/");
        final CnnMainPage mainPage= new CnnMainPage(webDriver);
        mainPage.waitForJsToLoad();
        mainPage.getSearchField();
        final CnnSearchResultPage searchResultPage= mainPage.searchFor("bitcoin");
        List<String> searchResultsTitles = searchResultPage.getSearchResultTitles();
        List<String> searchResultsBody = searchResultPage.getSearchResultBody();

        List<CnnDto> webResultList = new ArrayList<>();
        for (int i = 0; i < searchResultsBody.size(); i++) {
            CnnDto cnnDto= new CnnDto(searchResultsBody.get(i),searchResultsTitles.get(i));
            webResultList.add(cnnDto);
        }


        CnnApi api = new CnnApi();
        CnnResponseResult response = api.searchFor();
        List<CnnDto> results = response.getResult();

        Assert.assertEquals(webResultList.toString().replaceAll(" ", ""),
                results.toString().replaceAll("\n", "").replaceAll(" ", ""));
    }
}
